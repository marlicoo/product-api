Feature: Cart
  Scenario: Create new cart
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts" with body:
    """
    {
      "id": "2a249768-0706-415f-a032-08c367189653"
    }
    """
    Then the response status code should be 201
    And the output should contain:
    """
    {
     "_links": {
        "self": {
          "href": "/carts/2a249768-0706-415f-a032-08c363889430"
              },
        "products": {
          "href": "/products"
              },
         }
     }
     """


  Scenario: Add product to cart
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts/2a249768-0706-415f-a032-08c367189653/products" with body:
    """
    {
      "productId": "513c6458-25f2-4372-9e12-b2428c8019bb"
    }
    """
    Then the response status code should be 200
    And the output should contain:
    """
    {
     "_links": {
        "self": {
          "href": "/carts/2a249768-0706-415f-a032-08c363889430"
              },
        "products": {
          "href": "/products"
              },
         }
     }
     """


  Scenario: Add second product to cart
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts/2a249768-0706-415f-a032-08c367189653/products" with body:
    """
    {
      "productId": "613c6458-25f2-4372-9e12-b2428c8019bb"
    }
    """
    Then the response status code should be 200
    And the output should contain:
    """
    {
     "_links": {
        "self": {
          "href": "/carts/2a249768-0706-415f-a032-08c363889430"
              },
        "products": {
          "href": "/products"
              },
         }
     }
     """


  Scenario: Add third product to cart
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts/2a249768-0706-415f-a032-08c367189653/products" with body:
    """
    {
      "productId": "713c6458-25f2-4372-9e12-b2428c8019bb"
    }
    """
    Then the response status code should be 200
    And the output should contain:
    """
    {
     "_links": {
        "self": {
          "href": "/carts/2a249768-0706-415f-a032-08c363889430"
              },
        "products": {
          "href": "/products"
              },
         }
     }
     """


  Scenario: Add fourth product to cart - exceeding the limit of products
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts/2a249768-0706-415f-a032-08c367189653/products" with body:
    """
    {
      "productId": "813c6458-25f2-4372-9e12-b2428c8019bb"
    }
    """
    Then the response status code should be 400
    And the JSON nodes should contain:
      | error                   | Limit 3 of product in cart has been expired              |


  Scenario: Remove product from cart
    When I add "Content-Type" header equal to "application/json"
    And I send a "DELETE" request to "/api/carts/2a249768-0706-415f-a032-08c367189653/products/713c6458-25f2-4372-9e12-b2428c8019bb"
    Then the response status code should be 200


  Scenario: Get cart with products
    When I add "Content-Type" header equal to "application/json"
    And I send a "GET" request to "/api/carts/2a249768-0706-415f-a032-08c367189653"
    Then the response status code should be 200
    And the output should contain:
    """
    {
    "valueOfCart": "4.98 USD",
    "_links": {
        "self": {
            "href": "/carts/2a249768-0706-415f-a032-08c367189653"
        },
        "products": {
            "href": "/products"
        }
    },
    "_embedded": {
        "product": [
            {
                "id": {},
                "title": "Fallout",
                "price": "1.99 USD",
                "_links": {
                    "self": {
                        "href": "products/513c6458-25f2-4372-9e12-b2428c8019bb"
                    }
                }
            },
            {
                "id": {},
                "title": "Don`t Starve",
                "price": "2.99 USD",
                "_links": {
                    "self": {
                        "href": "products/613c6458-25f2-4372-9e12-b2428c8019bb"
                    }
                }
            }
        ]
      }
     }
     """

    # Error requests

  Scenario: Create cart with invalid uuid
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts" with body:
    """
    {
      "id": "813c6458"
    }
    """
    Then the response status code should be 400
    And the output should contain:
    """
    {
     "error": "Id 2a249768-0706-415f-a032-08c36718 is not valid uuid type"
     }
     """

  Scenario: Create cart with no id param provided
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/carts" with body:
    """
    {
      "id": "813c6458"
    }
    """
    Then the response status code should be 400
    And the output should contain:
    """
    {
    "validationErrors": [
        [
            {
                "error": "key not provided",
                "path": "id"
            }
        ]
    ]
}
     """


