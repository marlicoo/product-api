Feature: Product
  Scenario: Create new product
    When I add "Content-Type" header equal to "application/json"
    And I send a "POST" request to "/api/products" with body:
    """
    {
      "id" : "2a249768-0706-415f-a032-08c367189653",
	  "title": "super",
	  "currency": "USD",
	  "amount": "21.99"
    }
    """
    Then the response status code should be 201
    And the output should contain:
    """
    {
    "title": "super",
    "price": "21.99 USD",
    "_links": {
        "self": {
            "href": "/products/2a249768-0706-415f-a032-08c367189653"
        },
        "products": {
            "href": "/products"
        }
      }
     }
     """

  Scenario: Update product price
    When I add "Content-Type" header equal to "application/json"
    And I send a "PUT" request to "/api/products/013c6458-25f2-4372-9e12-b2428c8019bb" with body:
    """
    {
      "amount": "9.99",
      "currency": "USD"
    }
    """
    Then the response status code should be 200

  Scenario: Update product title
    When I add "Content-Type" header equal to "application/json"
    And I send a "PUT" request to "/api/products/013c6458-25f2-4372-9e12-b2428c8019bb" with body:
    """
    {
      "title": "Dragon 2"
    }
    """
    Then the response status code should be 200

  Scenario: Update product title and price
    When I add "Content-Type" header equal to "application/json"
    And I send a "PUT" request to "/api/products/013c6458-25f2-4372-9e12-b2428c8019bb" with body:
    """
    {
      "title": "Dragon",
      "amount": "5.99",
      "currency": "USD"
    }
    """
    Then the response status code should be 200


  Scenario: Remove product
    When I add "Content-Type" header equal to "application/json"
    And I send a "DELETE" request to "/api/products/013c6458-25f2-4372-9e12-b2428c8019bb"
    Then the response status code should be 204


  Scenario: Get product list page 1
    When I add "Content-Type" header equal to "application/json"
    And I send a "GET" request to "/api/products"
    Then the response status code should be 200
    And the output should contain:
    """
    {
    "limit": 3,
    "page": 1,
    "_links": {
        "self": {
            "href": "/products"
        },
        "next": {
            "href": "/products?page=2&&limit=3"
        }
    },
    "_embedded": {
        "product": [
            {
                "id": "2a249768-0706-415f-a032-08c367189653",
                "title": "super",
                "price": "21.99 USD",
                "_links": {
                    "self": {
                        "href": "/products/2a249768-0706-415f-a032-08c367189653"
                    }
                }
            },
            {
                "id": "513c6458-25f2-4372-9e12-b2428c8019bb",
                "title": "Fallout",
                "price": "1.99 USD",
                "_links": {
                    "self": {
                        "href": "/products/513c6458-25f2-4372-9e12-b2428c8019bb"
                    }
                }
            },
            {
                "id": "613c6458-25f2-4372-9e12-b2428c8019bb",
                "title": "Don`t Starve",
                "price": "2.99 USD",
                "_links": {
                    "self": {
                        "href": "/products/613c6458-25f2-4372-9e12-b2428c8019bb"
                    }
                }
            }
        ]
      }
     }
     """

