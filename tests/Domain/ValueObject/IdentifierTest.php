<?php

namespace App\Tests\Domain\ValueObject;


use App\Domain\Exception\IdentifierInvalidArgument;
use App\Domain\ValueObject\Identifier;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class IdentifierTest extends TestCase
{
    public function testCanBeCreatedForValidUuid()
    {
        $this->assertInstanceOf(
            Identifier::class,
            Identifier::UUID('813c6458-25f2-4372-9e12-b2428c8019bb'));
    }

    public function testCannotBeCreatedForInvalidUuid()
    {
        $this->expectException(IdentifierInvalidArgument::class);
        Identifier::UUID('813c6458');
    }
}
