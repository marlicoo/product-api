<?php

namespace App\Tests\Domain\ValueObject;


use App\Domain\Exception\MoneyInvalidArgument;
use App\Domain\ValueObject\Money;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class MoneyTest extends TestCase
{
    public function testCanBeCreatedForUsdAndValidAmount()
    {
        $this->assertInstanceOf(
            Money::class,
            Money::USD(21.99));
    }

    public function testCanBeCreatedForEurAndValidAmount()
    {
        $this->assertInstanceOf(
            Money::class,
            Money::EUR(21.99));
    }

    public function testCanBeCreatedForPlnAndValidAmount()
    {
        $this->assertInstanceOf(
            Money::class,
            Money::PLN(21.99));
    }

    public function testCannotBeCreatedForPlnAndNegativeAmount()
    {
        $this->expectException(MoneyInvalidArgument::class);
        Money::PLN(-12.99);
    }

    public function testCannotBeCreatedForUsdAndNegativeAmount()
    {
        $this->expectException(MoneyInvalidArgument::class);
        Money::USD(-12.99);
    }

    public function testCannotBeCreatedForEurAndNegativeAmount()
    {
        $this->expectException(MoneyInvalidArgument::class);
        Money::EUR(-12.99);
    }
}
