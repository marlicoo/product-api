### Installation:
1. `docker-compose up -d`
2. `docker-compose exec php-fpm bash`
3. `composer install`
4. `php bin/console doctrine:migrations:migrate`
5. `php bin/console doctrine:fixtures:load`
6. run tests `vendor/bin/behat`
7. Check api documentation [api documentation](http://localhost:8003/api/doc)

#### Notice: If database error occur check .env file and try change database url to 
`DATABASE_URL=mysql://dbuser:dbpw@mysql:3306/product_api`

#### Domain logic notice:
1. Cart and Product id are uuid type
2. Uuid are provided by client side