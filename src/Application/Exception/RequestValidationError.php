<?php

namespace App\Application\Exception;


class RequestValidationError extends \RuntimeException implements ValidationException
{
    /** @var array */
    private $errors;

    /**
     * @param array $errors
     * @return RequestValidationError
     */
    public static function forNullParams(array $errors): RequestValidationError
    {
        $exception = new self();
        $exception->setErrors($errors);

        return $exception;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}
