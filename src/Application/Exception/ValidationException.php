<?php

namespace App\Application\Exception;


interface ValidationException
{
    public function getMessage();

    public function getErrors();
}