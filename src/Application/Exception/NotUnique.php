<?php

namespace App\Application\Exception;


use App\Domain\ValueObject\Identifier;
use Zend\Code\Exception\RuntimeException;

class NotUnique extends RuntimeException implements ApplicationException
{
    /**
     * @param Identifier $id
     * @return NotUnique
     */
    public static function forId(Identifier $id): NotUnique
    {
        return new self(sprintf('Resource with id %s already exist', $id));
    }
}
