<?php

namespace App\Application\Exception;


interface ApplicationException
{
    public function getMessage();
}
