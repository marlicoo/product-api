<?php

namespace App\Application\Exception;


class RequestException extends \RuntimeException implements ApplicationException
{

    /**
     * @param string $contentType
     * @return RequestException
     */
    public static function forContentType(string $contentType): RequestException
    {
        return new self(sprintf('Unsupported Content Type %s', $contentType));
    }
}
