<?php

namespace App\Application\Exception;


use App\Domain\ValueObject\Identifier;

class ResourceNotFound extends \RuntimeException implements ApplicationException
{
    /**
     * @param Identifier $id
     * @param string $resourceType
     * @return ResourceNotFound
     */
    public static function forResourceTypeAndId(Identifier $id, string $resourceType): ResourceNotFound
    {
        return new self(sprintf('%s with id %s does not exist', ucfirst($resourceType), $id));
    }
}
