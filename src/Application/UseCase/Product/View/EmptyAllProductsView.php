<?php

namespace App\Application\UseCase\Product\View;

use App\Application\Enum\Pagination;
use App\Application\UseCase\ResourceListView;

class EmptyAllProductsView implements ResourceListView
{
    /**
     * @return array
     */
    public function resources(): array
    {
        return [];
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function nextPage(): int
    {
        return Pagination::NO_EXIST_NEXT_PAGE;
    }

    /**
     * @return int
     */
    public function previousPage(): int
    {
        return Pagination::NO_EXIST_PREVIOUS_PAGE;
    }
}
