<?php

namespace App\Application\UseCase\Product\View;

use App\Application\UseCase\ResourceListView;
use App\Domain\Model\Product\Product;

class AllProductsView implements ResourceListView
{
    /** @var Object[] */
    private $products;

    /** @var int */
    private $limit;

    /** @var int */
    private $page;

    /** @var  int */
    private $nextPage;

    /** @var int*/
    private $previousPage;

    /**
     * AllProductsView constructor.
     * @param Object[] $products
     * @param int $limit
     * @param int $page
     */
    public function __construct(array $products, $limit, $page)
    {
        $this->products = $products;
        $this->limit = $limit;
        $this->page = $page;
    }

    /**
     * @return Product[]
     */
    public function resources(): array
    {
        return $this->products;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function nextPage(): int
    {
        return $this->nextPage;
    }

    /**
     * @return int
     */
    public function previousPage(): int
    {
        return $this->previousPage;
    }

    /**
     * @param int $previousPage
     */
    public function setPreviousPage(int $previousPage)
    {
        $this->previousPage = $previousPage;
    }

    /**
     * @param int $nextPage
     */
    public function setNextPage(int $nextPage)
    {
        $this->nextPage = $nextPage;
    }
}

