<?php

namespace App\Application\UseCase\Product\View;


use App\Application\UseCase\View;
use App\Domain\ValueObject\Identifier;

class ProductView implements View
{
    /** @var  Identifier */
private $id;

/** @var  string */
private $title;

/** @var string */
private $price;

    /**
     * ProductView constructor.
     * @param Identifier $id
     * @param string $title
     * @param string $price
     */
    public function __construct(Identifier $id, $title, $price)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function price(): string
    {
        return $this->price;
    }
}
