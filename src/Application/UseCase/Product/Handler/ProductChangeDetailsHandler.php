<?php

namespace App\Application\UseCase\Product\Handler;


use App\Application\UseCase\Product\Command\ProductChangeDetails;
use App\Domain\Exception\MoneyInvalidArgument;
use App\Domain\Service\ProductManager;

class ProductChangeDetailsHandler
{
    /** @var  ProductManager */
    private $productManager;

    /**
     * ProductCreateHandler constructor.
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param ProductChangeDetails $command
     * @throws MoneyInvalidArgument
     */
    public function handle(ProductChangeDetails $command): void
    {
        $this->productManager->changeProductDetails(
            $command->id(),
            $command->title(),
            $command->amount(),
            $command->currency());
    }
}
