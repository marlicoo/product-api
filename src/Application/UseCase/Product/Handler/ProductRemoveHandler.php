<?php

namespace App\Application\UseCase\Product\Handler;


use App\Application\Exception\ResourceNotFound;
use App\Application\UseCase\Product\Command\ProductRemove;
use App\Domain\Exception\ProductCanNotBeRemoved;
use App\Domain\Service\ProductManager;


class ProductRemoveHandler
{
    /** @var  ProductManager */
    private $productManager;

    /**
     * ProductCreateHandler constructor.
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param ProductRemove $command
     * @throws ProductCanNotBeRemoved
     * @throws ResourceNotFound
     */
    public function handle(ProductRemove $command): void
    {
        $this->productManager->removeProduct($command->id());
    }
}
