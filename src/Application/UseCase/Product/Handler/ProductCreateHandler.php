<?php

namespace App\Application\UseCase\Product\Handler;


use App\Application\UseCase\Product\Command\ProductCreate;
use App\Domain\Exception\MoneyInvalidArgument;
use App\Domain\Service\ProductManager;


class ProductCreateHandler
{
    /** @var  ProductManager */
    private $productManager;

    /**
     * ProductCreateHandler constructor.
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param ProductCreate $command
     * @throws MoneyInvalidArgument
     */
    public function handle(ProductCreate $command): void
    {
        $this->productManager->newProduct($command->id(), $command->title(), $command->amount(), $command->currency());
    }
}
