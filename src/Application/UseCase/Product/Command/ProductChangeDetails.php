<?php

namespace App\Application\UseCase\Product\Command;


use App\Application\UseCase\Command;
use App\Domain\ValueObject\Currency;
use App\Domain\ValueObject\Identifier;

class ProductChangeDetails implements Command
{
    /** @var Identifier */
    private $id;

    /** @var string */
    private $title;

    /** @var float */
    private $amount;

    /** @var Currency */
    private $currency;

    /**
     * ProductCreate constructor.
     * @param Identifier $id
     * @param string $title

     */
    public function __construct(Identifier $id, $title = null, $amount = null, $currency = null)
    {
        $this->id = $id;
        $this->title = $title ?? '';
        $this->amount = $amount ?? 0.00;
        $this->currency = Currency::createForKey($currency) ?? Currency::NO_CHANGED();

    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return (float)$this->amount;
    }

    /**
     * @return Currency
     */
    public function currency(): Currency
    {
        return $this->currency;
    }

}