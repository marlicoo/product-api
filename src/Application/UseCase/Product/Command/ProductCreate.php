<?php

namespace App\Application\UseCase\Product\Command;


use App\Application\UseCase\Command;
use App\Domain\ValueObject\Currency;
use App\Domain\ValueObject\Identifier;
use Symfony\Component\Validator\Constraints as Assert;

class ProductCreate implements Command
{
    /** @var Identifier */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="No product title provided")
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="No product amount provided")
     */
    private $amount;

    /**
     * @var Currency
     * @Assert\NotBlank(message="No amount currency provided")
     */
    private $currency;

    /**
     * ProductCreate constructor.
     * @param Identifier $id
     * @param string $title
     * @param string $amount
     * @param Currency $currency
     */
    public function __construct(Identifier $id, $title, $amount, Currency $currency)
    {
        $this->id = $id;
        $this->title = $title;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function amount(): string
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function currency()
    {
        return $this->currency;
    }


}