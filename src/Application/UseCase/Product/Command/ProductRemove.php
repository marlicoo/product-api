<?php

namespace App\Application\UseCase\Product\Command;


use App\Application\UseCase\Command;
use App\Domain\ValueObject\Identifier;

class ProductRemove implements Command
{
    /** @var Identifier */
    private $id;

    /**
     * ProductCreate constructor.
     * @param Identifier $id
     */
    public function __construct(Identifier $id)
    {
        $this->id = $id;
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }
}