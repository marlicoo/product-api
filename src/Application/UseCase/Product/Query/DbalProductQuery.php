<?php

namespace App\Application\UseCase\Product\Query;


use App\Application\Service\PaginationService;
use App\Application\UseCase\Product\Query\Filter\PaginationFilter;
use App\Application\UseCase\Product\View\AllProductsView;
use App\Application\UseCase\Product\View\EmptyAllProductsView;
use App\Application\UseCase\ResourceListView;
use App\Domain\Model\Product\ProductStatusType;
use Doctrine\DBAL\Driver\Connection;


class DbalProductQuery implements ProductQuery
{
    /** @var Connection */
    private $connection;

    /**
     * DbalProductQuery constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param PaginationFilter $filter
     * @return ResourceListView
     */
    public function findAll(PaginationFilter $filter): ResourceListView
    {
        $limit = $filter->limit();
        $page = $filter->page();
        $all = $this->countProducts();

        $paginationService = PaginationService::create((int)$all, $page, $limit);

        if (!$paginationService->isPageExist()) {
            return new EmptyAllProductsView();
        }

        $statement = $this->connection->prepare(
            'SELECT id, title, price FROM product WHERE product.status=:status LIMIT :limit OFFSET :offset');
        $statement->bindValue('status', ProductStatusType::ACTIVE);
        $statement->bindValue('limit', $paginationService->getLimit(), \PDO::PARAM_INT);
        $statement->bindValue('offset', $paginationService->getOffset(), \PDO::PARAM_INT);
        $statement->execute();

        $view = new AllProductsView($statement->fetchAll(\PDO::FETCH_OBJ), $limit, $page);
        $view->setNextPage($paginationService->nextPage());
        $view->setPreviousPage($paginationService->previousPage());

        return $view;
    }

    /**
     * @return string
     */
    public function countProducts(): string
    {
        $statement = $this->connection->prepare(
            'SELECT COUNT(id) FROM product WHERE product.status=:status');
        $statement->bindValue('status', ProductStatusType::ACTIVE);
        $statement->execute();

        return $statement->fetchColumn()?? 0;
    }
}
