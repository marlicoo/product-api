<?php

namespace App\Application\UseCase\Product\Query\Filter;


class PaginationFilter implements ProductFilter
{
    /** @var int */
    private $limit;
    /** @var  int */
    private $page;

    /**
     * DbalProductQuery constructor.
     * @param int $limit
     * @param int $page
     */
    public function __construct($limit = null, $page = null)
    {
        $this->limit = $limit ?? 3;
        $this->page = $page ?? 1;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }
}
