<?php

namespace App\Application\UseCase\Product\Query;


use App\Application\UseCase\Product\Query\Filter\PaginationFilter;
use App\Application\UseCase\ResourceListView;


interface ProductQuery
{
    public function findAll(PaginationFilter $filter): ResourceListView;
}