<?php

namespace App\Application\UseCase\Product\Middleware;

use League\Tactician\Middleware;

class ProductRemoveMiddleware implements Middleware
{
    /**
     * @param object $command
     * @param callable $next
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        return $next($command);
    }
}
