<?php

namespace App\Application\UseCase\Product\Middleware;


use App\Application\Exception\RequestValidationError;
use App\Application\Exception\ResourceNotFound;
use App\Application\Service\Validation\ErrorMessageManager;
use App\Application\UseCase\Product\Command\ProductChangeDetails;
use App\Domain\ValueObject\Currency;
use League\Tactician\Middleware;

class ProductChangeDetailsMiddleware implements Middleware
{
    /**
     * @param object $command
     * @param callable $next
     * @return mixed
     * @throws RequestValidationError
     * @throws ResourceNotFound
     */
    public function execute($command, callable $next)
    {
        if ($command instanceof ProductChangeDetails) {

            if ($command->currency()->equals(Currency::NO_CHANGED())
                && $command->amount() === 0.00
                && empty($command->title())) {
                throw RequestValidationError::forNullParams(
                            ErrorMessageManager::generateErrorArray(['title', 'price', 'currency'],
                        ErrorMessageManager::FOR_NULL));
            }

        }

        return $next($command);
    }
}
