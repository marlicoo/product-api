<?php

namespace App\Application\UseCase\Product\Middleware;


use App\Application\Exception\NotUnique;
use App\Application\Exception\ResourceNotFound;
use App\Application\UseCase\Product\Command\ProductCreate;
use App\Domain\Model\Product\Product;
use App\Infrastructure\Repository\DoctrineOrmProductRepository;
use League\Tactician\Middleware;

class ProductCreateMiddleware implements Middleware
{
    /** @var  DoctrineOrmProductRepository */
    private $productRepository;

    /**
     * CartCreateHandler constructor.
     * @param DoctrineOrmProductRepository $orderRepository
     */
    public function __construct(DoctrineOrmProductRepository $orderRepository)
    {
        $this->productRepository = $orderRepository;
    }

    /**
     * @param object $command
     * @param callable $next
     * @return mixed
     * @throws ResourceNotFound
     * @throws NotUnique
     */
    public function execute($command, callable $next)
    {
        if ($command instanceof ProductCreate) {

            try {
                $this->productRepository->findById($command->id());
            } catch (ResourceNotFound $exception) {
                return $next($command);
            }

            throw NotUnique::forId($command->id());
        }

        return $next($command);
    }
}
