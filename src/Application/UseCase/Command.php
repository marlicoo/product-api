<?php

namespace App\Application\UseCase;


interface Command
{
    public function id();
}