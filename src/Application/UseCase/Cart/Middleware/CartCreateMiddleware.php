<?php

namespace App\Application\UseCase\Cart\Middleware;


use App\Application\Exception\NotUnique;
use App\Application\Exception\ResourceNotFound;
use App\Application\UseCase\Cart\Command\CartCreate;
use App\Domain\Model\Cart\Cart;
use App\Infrastructure\Repository\DoctrineOrmCartRepository;
use League\Tactician\Middleware;

class CartCreateMiddleware implements Middleware
{
    /** @var  DoctrineOrmCartRepository */
    private $cartRepository;

    /**
     * CartCreateHandler constructor.
     * @param DoctrineOrmCartRepository $orderRepository
     */
    public function __construct(DoctrineOrmCartRepository $orderRepository)
    {
        $this->cartRepository = $orderRepository;
    }

    /**
     * @param object $command
     * @param callable $next
     * @return mixed
     * @throws ResourceNotFound
     * @throws NotUnique
     */
    public function execute($command, callable $next)
    {
        if ($command instanceof CartCreate) {

            try {
                $this->cartRepository->findById($command->id());
            } catch (ResourceNotFound $exception) {
                return $next($command);
            }

            throw NotUnique::forId($command->id());
        }

        return $next($command);
    }
}
