<?php

namespace App\Application\UseCase\Cart\Command;


use App\Application\UseCase\Command;
use App\Domain\ValueObject\Identifier;
use Symfony\Component\Validator\Constraints as Assert;

class CartCreate implements Command
{
    /**
     * @var Identifier
     */
    private $id;

    /**
     * CartCreate constructor.
     * @param Identifier $id
     */
    public function __construct(Identifier $id)
    {
        $this->id = $id;
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }
}
