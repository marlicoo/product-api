<?php

namespace App\Application\UseCase\Cart\Command;


use App\Application\UseCase\Command;
use App\Domain\ValueObject\Identifier;

class AddProduct implements Command
{
    /** @var Identifier */
    private $id;

    /** @var Identifier */
    private $productId;

    /**
     * AddProduct constructor.
     * @param Identifier $id
     * @param Identifier $productId
     */
    public function __construct(Identifier $id, Identifier $productId)
    {
        $this->id = $id;
        $this->productId = $productId;
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }

    /**
     * @return Identifier
     */
    public function productId(): Identifier
    {
        return $this->productId;
    }
}
