<?php

namespace App\Application\UseCase\Cart\View;


use App\Application\UseCase\Product\View\ProductView;
use App\Application\UseCase\View;
use App\Domain\ValueObject\Identifier;

class CartWithProductsView implements View
{
    /** @var ProductView[] */
    private $products;

    /** @var string */
    private $valueOfCart;

    /** @var  Identifier */
    private $id;

    /**
     * CartWithProductsView constructor.
     * @param ProductView[] $products
     * @param string $valueOfCart
     * @param Identifier $id
     */
    public function __construct(array $products, $valueOfCart, Identifier $id)
    {
        $this->products = $products;
        $this->valueOfCart = $valueOfCart;
        $this->id = $id;
    }

    /**
     * @return ProductView[]
     */
    public function products(): array
    {
        return $this->products;
    }

    /**
     * @return string
     */
    public function valueOfCart(): string
    {
        return $this->valueOfCart;
    }

    /**
     * @return Identifier
     */
    public function id(): Identifier
    {
        return $this->id;
    }
}