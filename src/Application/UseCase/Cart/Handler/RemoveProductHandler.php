<?php

namespace App\Application\UseCase\Cart\Handler;


use App\Application\Exception\ResourceNotFound;
use App\Application\UseCase\Cart\Command\RemoveProduct;
use App\Domain\Service\CartManager;


class RemoveProductHandler
{
    /** @var CartManager */
    private $cartManager;

    /**
     * CartCreateHandler constructor.
     * @param CartManager $cartManager
     */
    public function __construct(CartManager $cartManager)
    {
        $this->cartManager = $cartManager;
    }

    /**
     * @param RemoveProduct $command
     * @throws ResourceNotFound
     */
    public function handle(RemoveProduct $command): void
    {
       $this->cartManager->removeProduct($command->id(), $command->productId());
    }
}