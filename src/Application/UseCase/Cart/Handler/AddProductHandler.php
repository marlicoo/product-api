<?php

namespace App\Application\UseCase\Cart\Handler;


use App\Application\Exception\ResourceNotFound;
use App\Application\UseCase\Cart\Command\AddProduct;
use App\Domain\Exception\CartProductLimitExpire;
use App\Domain\Service\CartManager;


class AddProductHandler
{
    /** @var CartManager */
    private $cartManager;

    /**
     * CartCreateHandler constructor.
     * @param CartManager $cartManager
     */
    public function __construct(CartManager $cartManager)
    {
        $this->cartManager = $cartManager;
    }

    /**
     * @param AddProduct $command
     * @throws ResourceNotFound
     * @throws CartProductLimitExpire
     */
    public function handle(AddProduct $command): void
    {
       $this->cartManager->addProduct($command->id(), $command->productId());
    }
}
