<?php

namespace App\Application\UseCase\Cart\Handler;


use App\Application\UseCase\Cart\Command\CartCreate;
use App\Domain\Service\CartManager;


class CartCreateHandler
{
    /** @var CartManager */
    private $cartManager;

    /**
     * CartCreateHandler constructor.
     * @param CartManager $cartManager
     */
    public function __construct(CartManager $cartManager)
    {
        $this->cartManager = $cartManager;
    }

    /**
     * @param CartCreate $command
     */
    public function handle(CartCreate $command): void
    {
       $this->cartManager->createCart($command->id());
    }
}

