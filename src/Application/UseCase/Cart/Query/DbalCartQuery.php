<?php

namespace App\Application\UseCase\Cart\Query;


use App\Application\Enum\Resource;
use App\Application\Exception\ResourceNotFound;
use App\Application\UseCase\Cart\View\CartWithProductsView;
use App\Application\UseCase\Product\View\ProductView;
use App\Application\UseCase\View;
use App\Domain\ValueObject\Identifier;
use Doctrine\DBAL\Driver\Connection;


class DbalCartQuery implements CartQuery
{
    /** @var Connection */
    private $connection;

    /**
     * DbalProductQuery constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param Identifier $cartId
     * @return View
     * @throws ResourceNotFound
     */
    public function cart(Identifier $cartId): View
    {
        #TODO remove statment
        $statement = $this->connection->prepare(
            'SELECT * FROM cart  WHERE id=:id');
        $statement->bindValue('id', $cartId);
        $statement->execute();

        if(!$statement->fetch()){
            throw ResourceNotFound::forResourceTypeAndId($cartId, Resource::CART());
        }

        $statement = $this->connection->prepare(
            'SELECT product_id, title, price FROM cart 
                        JOIN carts_products ON cart.id = carts_products.cart_id 
                        JOIN product ON carts_products.product_id = product.id
                        WHERE cart_id=:id');

        $statement->bindValue('id', $cartId);
        $statement->execute();
        $products = $statement->fetchAll(\PDO::FETCH_CLASS);

        $productsView = [];
        $totalValueOfCart = 0;

        foreach ($products as $product){

            list($amount, $currency) = explode(' ', $product->price, 2);
            $totalValueOfCart += $amount;

            $productsView[] = new ProductView(Identifier::UUID($product->product_id), $product->title, $product->price);
        }

        return new CartWithProductsView($productsView, $totalValueOfCart. ' ' . $currency, $cartId);
    }
}
