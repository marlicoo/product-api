<?php

namespace App\Application\UseCase\Cart\Query;


use App\Application\UseCase\View;
use App\Domain\ValueObject\Identifier;


interface CartQuery
{
    public function cart(Identifier $cartId): View;
}