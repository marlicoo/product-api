<?php

namespace App\Application\UseCase;


interface ResourceListView extends View
{
    /**
     * @return Object[]
     */
    public function resources(): array;

    /**
     * @return int
     */
    public function limit(): int;

    /**
     * @return int
     */
    public function page(): int;

    /**
     * @return int
     */
    public function nextPage(): int;

    /**
     * @return int
     */
    public function previousPage(): int;
}
