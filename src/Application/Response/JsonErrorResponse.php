<?php


namespace App\Application\Response;


use App\Application\Exception\ApplicationException;
use App\Application\Exception\ValidationException;
use App\Domain\Exception\DomainException;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonErrorResponse
{
    /**
     * @param DomainException $exception
     * @return JsonResponse
     */
    public static function domainError(DomainException $exception): JsonResponse
    {
        return JsonResponse::create(['error' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @param ApplicationException $exception
     * @return JsonResponse
     */
    public static function applicationError(ApplicationException $exception): JsonResponse
    {
        return JsonResponse::create(['error' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @param ValidationException $exception
     * @return JsonResponse
     */
    public static function validationErrors(ValidationException $exception): JsonResponse
    {
        return JsonResponse::create(['validationErrors' => [$exception->getErrors()]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param Exception $exception
     * @return JsonResponse
     */
    public static function undefinedError(Exception $exception): JsonResponse
    {
        return JsonResponse::create(['error' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
    }
}
