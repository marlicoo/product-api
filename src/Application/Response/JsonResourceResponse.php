<?php

namespace App\Application\Response;


use App\Application\Enum\Resource;
use App\Application\Service\Response\HalResponseResourceFactory;
use App\Application\UseCase\Command;
use App\Application\UseCase\ResourceListView;
use App\Application\UseCase\View;
use App\Domain\ValueObject\Identifier;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResourceResponse
{
    /**
     * @param Resource $resource
     * @param Command $command
     * @return JsonResponse
     */
    public static function created(Resource $resource, Command $command): JsonResponse
    {
        $hal = HalResponseResourceFactory::create($resource);

        return JsonResponse::create(
            $hal::prepareForPostAndPut($command),
            JsonResponse::HTTP_CREATED,
            ['Location' => sprintf('/%s/%s', $resource->getValue(), $command->id())]);
    }

    /**
     * @param Resource $resource
     * @param Command $command
     * @param Resource $addedResource
     * @param Identifier $addedResourceId
     * @return JsonResponse
     */
    public static function addedToResource(Resource $resource, Command $command, Resource $addedResource, Identifier $addedResourceId): JsonResponse
    {
        $hal = HalResponseResourceFactory::create($resource);

        return JsonResponse::create(
            $hal::prepareForPostAndPut($command),
            JsonResponse::HTTP_OK, [
            'Location' => sprintf('/%s/%s/%s/%s', $resource->getValue(), $command->id(), $addedResource->getValue(), $addedResourceId)]);
    }

    /**
     * @param Resource $resource
     * @param Command $command
     * @return JsonResponse
     */
    public static function removedFromResource(Resource $resource, Command $command): JsonResponse
    {
        $hal = HalResponseResourceFactory::create($resource);

        return JsonResponse::create(
            $hal::prepareForPostAndPut($command),
            JsonResponse::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public static function deleted(): JsonResponse
    {
        return JsonResponse::create(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @return JsonResponse
     */
    public static function updated(): JsonResponse
    {
        return JsonResponse::create(null, JsonResponse::HTTP_OK);
    }

    /**
     * @param ResourceListView $view
     * @param Resource $resource
     * @return JsonResponse
     */
    public static function resourceList(ResourceListView $view, Resource $resource): JsonResponse
    {
        $hal = HalResponseResourceFactory::create($resource);

        return JsonResponse::create(
            $hal::prepareForResourceList($view),
            JsonResponse::HTTP_OK);
    }


    public static function resource(View $view, Resource $resource): JsonResponse
    {
        $hal = HalResponseResourceFactory::create($resource);

        return JsonResponse::create(
            $hal::prepareForResource($view),
            JsonResponse::HTTP_OK);
    }
}
