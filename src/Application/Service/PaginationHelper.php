<?php

namespace App\Application\Service;

class PaginationHelper
{
    /**
     * @param $all
     * @param $limit
     * @return float
     */
    public static function numberOfPages($all, $limit): float
    {
        return ceil($all / $limit);
    }

    /**
     * @param $limit
     * @param $page
     * @return int
     */
    public static function offsetFromPageNumber($limit, $page): int
    {
        if((int)$page <= 0){
            return 0;
        }

        return $limit * ($page - 1);
    }
}
