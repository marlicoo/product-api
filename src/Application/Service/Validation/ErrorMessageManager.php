<?php

namespace App\Application\Service\Validation;


class ErrorMessageManager
{
    public const FOR_NULL = 'key not provided';
    public const FOR_EMPTY = 'required';
    private const KEY_ERROR = 'error';
    private const KEY_PATH = 'path';

    /**
     * @param array $fields
     * @param string $for
     * @return array
     */
    public static function generateErrorArray($fields, $for): array
    {
        $errors = [];

        foreach ($fields as $field) {
            $errors[] = [self::KEY_ERROR => $for, self::KEY_PATH => $field];
        }

        return $errors;
    }
}
