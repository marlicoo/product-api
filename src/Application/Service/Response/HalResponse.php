<?php

namespace App\Application\Service\Response;


use App\Application\UseCase\Command;
use App\Application\UseCase\ResourceListView;
use App\Application\UseCase\View;

interface HalResponse
{
   public static function prepareForPostAndPut(Command $command): array;

   public static function prepareForGet(Command $command): array;

   public static function prepareForResourceList(ResourceListView $view): array;

   public static function prepareForResource(View $view): array;
}
