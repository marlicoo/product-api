<?php

namespace App\Application\Service\Response;


use App\Application\Enum\Resource;

class HalResponseResourceFactory
{
    /**
     * @param Resource $resource
     * @return HalResponse
     */
    public static function create(Resource $resource)
    {
        switch ($resource->getValue()) {
            case Resource::PRODUCT:
                $hal = new ProductHalResponse();
                break;

            case Resource::CART:
                $hal = new CartHalResponse();
                break;

            default:
                $hal = new UndefinedResourceHalResponse();
        }

        return $hal;
    }
}
