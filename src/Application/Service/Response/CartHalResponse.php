<?php

namespace App\Application\Service\Response;


use App\Application\Enum\Resource;
use App\Application\UseCase\Cart\Command\AddProduct;
use App\Application\UseCase\Cart\Command\CartCreate;
use App\Application\UseCase\Cart\View\CartWithProductsView;
use App\Application\UseCase\Command;
use App\Application\UseCase\ResourceListView;
use App\Application\UseCase\View;
use Nocarrier\Hal;

class CartHalResponse implements HalResponse
{
    private const RESOURCE_BASE_PATH = '/carts';

    /**
     * @param Command $command
     * @return array
     * @throws \RuntimeException
     */
    public static function prepareForPostAndPut(Command $command): array
    {
        if (!$command instanceof CartCreate && !$command instanceof AddProduct) {
            return [];
        }

        $selfLink = sprintf(self::RESOURCE_BASE_PATH . '/%s', $command->id());

        $hal = new Hal($selfLink);
        $hal->addLink(Resource::PRODUCT, DIRECTORY_SEPARATOR . Resource::PRODUCT);

        $json = $hal->asJson(true);
        return (array)json_decode($json);
    }

    public static function prepareForResource(View $view): array
    {
        if (!$view instanceof CartWithProductsView) {
            return [];
        }

        $selfLink = sprintf(self::RESOURCE_BASE_PATH . '/%s', $view->id());

        $hal = new Hal($selfLink, ['valueOfCart' => $view->valueOfCart()]);
        $hal->addLink(Resource::PRODUCT, DIRECTORY_SEPARATOR . Resource::PRODUCT);

        foreach ($view->products() as $resource) {
            $product = new Hal(sprintf(Resource::PRODUCT . '/%s', $resource->id()),
                ['id' => $resource->id(), 'title' => $resource->title(), 'price' => $resource->price()]);

            $hal->addResource('product', $product);
        }

        $json = $hal->asJson(true);
        return (array)json_decode($json);
    }

    /**
     * @param Command $command
     * @return array
     */
    public static function prepareForGet(Command $command): array
    {
        // TODO: Implement prepareGetResponseBody() method.
    }

    /**
     * @param ResourceListView $view
     * @return array
     */
    public static function prepareForResourceList(ResourceListView $view): array
    {
        // TODO: Implement prepareGetResponseBody() method.
    }
}