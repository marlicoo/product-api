<?php

namespace App\Application\Service\Response;


use App\Application\UseCase\Command;
use App\Application\UseCase\ResourceListView;
use App\Application\UseCase\View;

class UndefinedResourceHalResponse implements HalResponse
{
    /**
     * @param Command $command
     * @return array
     */
    public static function prepareForPostAndPut(Command $command): array
    {
        return [];
    }

    /**
     * @param Command $command
     * @return array
     */
    public static function prepareForGet(Command $command): array
    {
        return [];
    }

    /**
     * @param ResourceListView $view
     * @return array
     */
    public static function prepareForResourceList(ResourceListView $view): array
    {
        return [];
    }

    /**
     * @param View $view
     * @return array
     */
    public static function prepareForResource(View $view): array
    {
        // TODO: Implement prepareForResource() method.
    }
}
