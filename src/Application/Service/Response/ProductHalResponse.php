<?php

namespace App\Application\Service\Response;


use App\Application\Enum\Pagination;
use App\Application\Enum\Resource;
use App\Application\UseCase\Command;
use App\Application\UseCase\ResourceListView;
use App\Application\UseCase\View;
use Nocarrier\Hal;

class ProductHalResponse implements HalResponse
{
    private const RESOURCE_BASE_PATH = '/products';

    /**
     * @param Command $command
     * @return array
     * @throws \RuntimeException
     */
    public static function prepareForPostAndPut(Command $command): array
    {
        $hal = new Hal(
            sprintf(self::RESOURCE_BASE_PATH . '/%s', $command->id()),
            ['title' => $command->title(), 'price' => $command->amount() . ' ' . $command->currency()]
        );

        $hal->addLink(Resource::PRODUCT, self::RESOURCE_BASE_PATH);

        $json = $hal->asJson(true);
        return (array)json_decode($json);
    }

    public static function prepareForGet(Command $command): array
    {
        return [];
    }

    /**
     * @param ResourceListView $view
     * @return array
     * @throws \RuntimeException
     */
    public static function prepareForResourceList(ResourceListView $view): array
    {
        $hal = new Hal(sprintf(self::RESOURCE_BASE_PATH), ['limit' => $view->limit(), 'page' => $view->page()]);

        if (!Pagination::isValid($view->nextPage())) {
            $hal->addLink(
                'next',
                self::RESOURCE_BASE_PATH . '?page=' . $view->nextPage() . '&&limit=' . $view->limit()
            );
        }

        if (!Pagination::isValid($view->previousPage())) {
            $hal->addLink('previous',
                self::RESOURCE_BASE_PATH . '?page=' . $view->previousPage() . '&&limit=' . $view->limit()
            );
        }

        foreach ($view->resources() as $resource) {
            $product = new Hal(sprintf(self::RESOURCE_BASE_PATH . '/%s', $resource->id),
                ['id' => $resource->id, 'title' => $resource->title, 'price' => $resource->price]);

            $hal->addResource('product', $product);
        }

        $json = $hal->asJson(true);
        return (array)json_decode($json);
    }

    /**
     * @param View $view
     * @return array
     */
    public static function prepareForResource(View $view): array
    {
        // TODO: Implement prepareForResource() method.

        return [];
    }


}
