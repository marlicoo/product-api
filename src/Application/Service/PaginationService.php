<?php

namespace App\Application\Service;


use App\Application\Enum\Pagination;

class PaginationService
{
    /** @var int */
    private $pages;

    /** @var int */
    private $page;

    /** @var int */
    private $offset;

    /** @var int */
    private $limit;

    /** @var int */
    private $numberOfResources;

    /**
     * PaginationService constructor.
     * @param $numberOfResources
     * @param $currentPage
     * @param $limit
     */
    public function __construct($numberOfResources, $currentPage, $limit)
    {
        $this->pages = PaginationHelper::numberOfPages($numberOfResources, $limit);
        $this->page = $currentPage;
        $this->offset = PaginationHelper::offsetFromPageNumber($limit, $currentPage);
        $this->limit = $limit;
        $this->numberOfResources = $numberOfResources;
    }

    /**
     * @param int $numberOfResources
     * @param int $currentPage
     * @param int $limit
     * @return PaginationService
     */
    public static function create($numberOfResources, $currentPage, $limit): PaginationService
    {
        return new self($numberOfResources, $currentPage, $limit);
    }

    /**
     * @return bool
     */
    public function isPageExist(): bool
    {
        return $this->page < $this->pages;
    }

    /**
     * @return int
     */
    public function nextPage(): int
    {
        if ($this->page > 0 && $this->page < $this->pages) {
            return $this->page + 1;
        }

        return Pagination::NO_EXIST_NEXT_PAGE;
    }

    /**
     * @return int
     */
    public function previousPage(): int
    {
        return $this->page > 1 ? $this->page -1 : Pagination::NO_EXIST_PREVIOUS_PAGE;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit > $this->numberOfResources ? $this->numberOfResources : $this->limit;
    }
}
