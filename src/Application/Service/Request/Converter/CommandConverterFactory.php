<?php

namespace App\Application\Service\Request\Converter;


use App\Application\Exception\RequestValidationError;
use App\Application\Service\Validation\ErrorMessageManager;
use App\Application\UseCase\Command;
use ArgumentCountError;
use ReflectionClass;

class CommandConverterFactory implements Converter
{
    /** @var Command[] */
    private $commands;

    /**
     * CommandConverterFactory constructor.
     * @param Command[] $commands
     */
    public function __construct(array $commands)
    {
        $this->commands = $commands;
    }

    /**
     * @param $className
     * @param array $data
     * @return Command
     * @throws RequestValidationError
     */
    public function convert($className, $data): Command
    {
        /** @var Command $reflection */
        $reflection = new ReflectionClass($className);
        $params = $reflection->getConstructor()->getParameters();
        $sortedData = [];
        $errors = [];

        foreach ($params as $param) {

            if (!isset($data[$param->name])) {
                $errors[] = $param->name;

            } else {
                if (($param->getClass())) {
                    $class = new ReflectionClass($param->getClass()->name);
                    $sortedData[$param->name] = $class->newInstance($data[$param->name]);
                } else {
                    $sortedData[$param->name] = $data[$param->name];
                }
            }
        }

        try {
            $command = $reflection->newInstanceArgs($sortedData);
        } catch (ArgumentCountError $exception) {
            throw RequestValidationError::forNullParams(
                ErrorMessageManager::generateErrorArray($errors, ErrorMessageManager::FOR_NULL));
        }

        return $command;
    }

    /**
     * @return Command[]
     */
    public function commands(): array
    {
        return $this->commands;
    }
}
