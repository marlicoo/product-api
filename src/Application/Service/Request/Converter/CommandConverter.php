<?php

namespace App\Application\Service\Request\Converter;


use App\Application\Exception\RequestException;
use App\Application\Service\Request\Encoder\BodyContentTypeEncoderFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class CommandConverter implements ParamConverterInterface
{
    /** @var CommandConverterFactory */
    private $commandFactory;

    private $encoder;

    /**
     * Converter constructor.
     * @param CommandConverterFactory $commandFactory
     * @param BodyContentTypeEncoderFactory $encoder
     */
    public function __construct(CommandConverterFactory $commandFactory, BodyContentTypeEncoderFactory $encoder)
    {
        $this->commandFactory = $commandFactory;
        $this->encoder = $encoder;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool|void
     * @throws RequestException
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $encoder = $this->encoder->createForFormat($request->getContentType());
        $parameters = $encoder::encode($request);

        if (null !== $request->attributes->get('id')) {
            $parameters['id'] = $request->attributes->get('id');
        }

        if (null !== $request->attributes->get('productId')) {
            $parameters['productId'] = $request->attributes->get('productId');
        }

        $command = $this->commandFactory->convert($configuration->getClass(), $parameters);

        $request->attributes->set($configuration->getName(), $command);
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration): bool
    {
        return \in_array($configuration->getClass(), $this->commandFactory->commands(), true);
    }
}
