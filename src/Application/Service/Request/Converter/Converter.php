<?php

namespace App\Application\Service\Request\Converter;


use App\Application\UseCase\Command;

interface Converter
{
    public function convert($className, $data): Command;
}
