<?php

namespace App\Application\Service\Request\Encoder;


use Symfony\Component\HttpFoundation\Request;

interface BodyContentTypeEncoder
{
    public static function encode(Request $request): array;
}
