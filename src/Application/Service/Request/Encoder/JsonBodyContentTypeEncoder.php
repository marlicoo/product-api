<?php

namespace App\Application\Service\Request\Encoder;


use Symfony\Component\HttpFoundation\Request;

class JsonBodyContentTypeEncoder implements BodyContentTypeEncoder
{
    /**
     * @param Request $request
     * @return array
     */
    public static function encode(Request $request): array
    {
        $parameters = json_decode($request->getContent(), true);

        return $parameters ?? [];
    }
}
