<?php


namespace App\Application\Service\Request\Encoder;


use App\Application\Exception\RequestException;

class BodyContentTypeEncoderFactory
{
    /** @var array */
    private $factories = [];

    /**
     * @param $format
     * @param $factory
     */
    public function addEncoderFactory($format, $factory): void
    {
        $this->factories[$format] = $factory;
    }

    /**
     * @param string $format
     * @return BodyContentTypeEncoder
     * @throws RequestException
     */
    public function createForFormat($format): BodyContentTypeEncoder
    {
        if (!isset($this->factories[$format])) {
            throw RequestException::forContentType($format ?? ' ');
        }

        return $this->factories[$format];
    }
}
