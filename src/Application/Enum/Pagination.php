<?php

namespace App\Application\Enum;


use MyCLabs\Enum\Enum;

/**
 * @method static Pagination NO_EXIST_NEXT_PAGE
 * @method static Pagination NO_EXIST_PREVIOUS_PAGE
 */
class Pagination extends Enum
{
    public const NO_EXIST_NEXT_PAGE = -1;
    public const NO_EXIST_PREVIOUS_PAGE = -2;
}
