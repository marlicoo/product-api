<?php

namespace App\Application\Enum;


use MyCLabs\Enum\Enum;

/**
 * @method static Resource PRODUCT()
 * @method static Resource CART()
 */
class Resource extends Enum
{
    public const PRODUCT = 'products';
    public const CART = 'carts';
}