<?php

namespace App\Application\Listener;


use App\Application\Exception\ApplicationException;
use App\Application\Exception\ValidationException;
use App\Application\Response\JsonErrorResponse;
use App\Domain\Exception\DomainException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();

        switch(true) {
            case $exception instanceof ValidationException:
                $response = JsonErrorResponse::validationErrors($exception);
                break;

            case $exception instanceof DomainException:
                $response = JsonErrorResponse::domainError($exception);
                break;

            case $exception instanceof ApplicationException:
                $response = JsonErrorResponse::applicationError($exception);
                break;
            default:
                $response = JsonErrorResponse::undefinedError($exception);
        }

       $event->setResponse($response);
    }
}
