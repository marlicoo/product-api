<?php

namespace App\Domain\Service\Currency;


use App\Domain\ValueObject\Currency;

class DefaultSupportedCurrency implements SupportedCurrency
{
    /** @var array */
    private $currencies;

    /**
     * DefaultSupportedCurrency constructor.
     * @param array $currencies
     */
    public function __construct($currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * @return Currency[]
     */
    public function supported(): array
    {
        $supported = [];

        foreach ($this->currencies as $currency) {
            $supported[] = Currency::createForKey($currency);
        }

        return $supported;
    }

    /**
     * @param Currency $currency
     * @return bool
     */
    public function isSupported(Currency $currency): bool
    {
        /** @var Currency[] $supportedCurrencies */
        $supportedCurrencies = $this->supported();

        foreach ($supportedCurrencies as $supportedCurrency) {
            if ($supportedCurrency->equals($currency)) {
                return true;
            }
        }

        return false;
    }
}

