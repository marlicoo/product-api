<?php

namespace App\Domain\Service\Currency;


use App\Domain\ValueObject\Currency;

interface SupportedCurrency
{
    /** @return Currency[] */
    public function supported(): array;

    public function isSupported(Currency $currency): bool;
}
