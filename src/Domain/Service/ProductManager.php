<?php

namespace App\Domain\Service;


use App\Application\Exception\ResourceNotFound;
use App\Domain\Exception\MoneyInvalidArgument;
use App\Domain\Exception\ProductCanNotBeRemoved;
use App\Domain\Model\Product\Product;
use App\Domain\Model\Product\ProductStatusType;
use App\Domain\Repository\ProductRepository;
use App\Domain\Service\Currency\SupportedCurrency;
use App\Domain\ValueObject\Currency;
use App\Domain\ValueObject\Identifier;
use App\Domain\ValueObject\Money;

class ProductManager
{
    /** @var ProductRepository */
    private $repository;

    /** @var SupportedCurrency */
    private $supportedCurrency;

    /**
     * ProductManager constructor.
     * @param ProductRepository $repository
     * @param SupportedCurrency $supportedCurrency
     */
    public function __construct(ProductRepository $repository, SupportedCurrency $supportedCurrency)
    {
        $this->repository = $repository;
        $this->supportedCurrency = $supportedCurrency;
    }

    /**
     * @param Identifier $productId
     * @param $title
     * @param $amount
     * @param Currency $currency
     * @throws MoneyInvalidArgument
     */
    public function newProduct(Identifier $productId, $title, $amount, Currency $currency): void
    {
        if (!$this->supportedCurrency->isSupported($currency)) {
            throw MoneyInvalidArgument::forNotSupportedCurrency($currency->getValue());
        }

        $currencyString = $currency->getValue();
        $product = Product::create($productId, $title, Money::$currencyString($amount));

        $this->repository->create($product);
    }

    /**
     * @param Identifier $productId
     * @param $title
     * @param $amount
     * @param Currency $currency
     * @throws ResourceNotFound
     */
    public function changeProductDetails(Identifier $productId, $title, $amount, Currency $currency): void
    {
        /** @var Product $product */
        $product = $this->repository->findByIdAndStatus($productId, ProductStatusType::ACTIVE);
        $product->changeTitle($title);

        if ($this->supportedCurrency->isSupported($currency)) {
            $currency = $currency->getValue();
            $product->changePrice(Money::$currency($amount));
        }

        $this->repository->update($product);
    }

    /**
     * @param Identifier $productId
     * @throws ResourceNotFound
     * @throws ProductCanNotBeRemoved
     */
    public function removeProduct(Identifier $productId): void
    {
        /** @var Product $product */
        $product = $this->repository->findByIdAndStatus($productId, ProductStatusType::ACTIVE);

        if ($this->repository->isProductInCarts($productId)) {
            throw ProductCanNotBeRemoved::forId($productId);
        }

        $product->remove();
        $this->repository->update($product);
    }
}