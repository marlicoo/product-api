<?php

namespace App\Domain\Service\ProductLimit;


class DefaultCartProductLimit implements CartProductLimit
{
    private const LIMIT = 3;

    /**
     * @return int
     */
    public function limit(): int
    {
        return self::LIMIT;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)self::LIMIT;
    }
}
