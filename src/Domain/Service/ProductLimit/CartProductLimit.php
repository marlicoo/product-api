<?php

namespace App\Domain\Service\ProductLimit;


interface CartProductLimit
{
    public function limit(): int;
}
