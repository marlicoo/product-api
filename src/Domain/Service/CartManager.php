<?php

namespace App\Domain\Service;


use App\Application\Exception\ResourceNotFound;
use App\Domain\Exception\CartProductLimitExpire;
use App\Domain\Exception\ProductDuplicateInCart;
use App\Domain\Model\Cart\Cart;
use App\Domain\Model\Product\Product;
use App\Domain\Model\Product\ProductStatusType;
use App\Domain\Repository\CartRepository;
use App\Domain\Repository\ProductRepository;
use App\Domain\Service\ProductLimit\CartProductLimit;
use App\Domain\ValueObject\Identifier;

class CartManager
{
    /** @var CartRepository */
    private $repository;

    /** @var CartProductLimit */
    private $productLimitPolicy;

    /** @var ProductRepository */
    private $productRepository;

    /**
     * ProductManager constructor.
     * @param CartRepository $repository
     * @param CartProductLimit $productLimitPolicy
     * @param ProductRepository $productRepository
     */
    public function __construct(CartRepository $repository,
                                CartProductLimit $productLimitPolicy,
                                ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productLimitPolicy = $productLimitPolicy;
        $this->productRepository = $productRepository;
    }

    /**
     * @param Identifier $cartId
     */
    public function createCart(Identifier $cartId): void
    {
        $cart = Cart::create($cartId, $this->productLimitPolicy);

        $this->repository->create($cart);
    }

    /**
     * @param Identifier $id
     * @param Identifier $productId
     * @throws ProductDuplicateInCart
     * @throws ResourceNotFound
     * @throws CartProductLimitExpire
     */
    public function addProduct(Identifier $id, Identifier $productId): void
    {
        if ($this->repository->isProductInCart($id, $productId)) {
            throw ProductDuplicateInCart::forId($productId);
        }

        /** @var Cart $cart */
        $cart = $this->repository->findById($id);

        /** @var Product $product */
        $product = $this->productRepository->findByIdAndStatus($productId, ProductStatusType::ACTIVE);

        $cart->addProduct($product);
        $this->repository->update($cart);
    }

    /**
     * @param Identifier $id
     * @param Identifier $productId
     * @throws ResourceNotFound
     */
    public function removeProduct(Identifier $id, Identifier $productId): void
    {
        /** @var Cart $cart */
        $cart = $this->repository->findById($id);

        /** @var Product $product */
        $product = $this->productRepository->findById($productId);

        $cart->removeProduct($product);
        $this->repository->update($cart);
    }
}
