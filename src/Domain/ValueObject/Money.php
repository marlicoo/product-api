<?php

namespace App\Domain\ValueObject;


use App\Domain\Exception\MoneyInvalidArgument;

class Money
{
    /** @var float */
    private $amount;

    /** @var Currency */
    private $currency;

    /**
     * Money constructor.
     * @param float $amount
     * @param Currency $currency
     * @throws MoneyInvalidArgument
     */
    public function __construct(float $amount, Currency $currency)
    {
        if (!$this->isNegativeAmount($amount)) {
            throw MoneyInvalidArgument::forNegativeAmount($amount);
        }

        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @param float $amount
     * @return Money
     * @throws MoneyInvalidArgument
     */
    public static function USD(float $amount): Money
    {
        return new static($amount, Currency::USD());
    }

    /**
     * @param float $amount
     * @return Money
     * @throws MoneyInvalidArgument
     */
    public static function EUR(float $amount): Money
    {
        return new static($amount, Currency::EUR());
    }

    /**
     * @param float $amount
     * @return Money
     * @throws MoneyInvalidArgument
     */
    public static function PLN(float $amount): Money
    {
        return new static($amount, Currency::PLN());
    }

    /**
     * @param float $amount
     * @return bool
     */
    private function isNegativeAmount(float $amount): bool
    {
        return $amount > 0;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function currency(): Currency
    {
        return $this->currency;
    }

    public function __toString()
    {
        return $this->amount .' '. $this->currency;
    }
}
