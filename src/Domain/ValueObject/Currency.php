<?php

namespace App\Domain\ValueObject;


use MyCLabs\Enum\Enum;

/**
 * @method static Currency PLN()
 * @method static Currency EUR()
 * @method static Currency USD()
 * @method static Currency NO_CHANGED()
 */
class Currency extends Enum
{
    private const PLN = 'PLN';
    private const EUR = 'EUR';
    private const USD = 'USD';
    private const NO_CHANGED = 'Currency missing';

    /**
     * @param $key
     * @return Currency
     */
    public static function createForKey($key): Currency
    {
        switch ($key){

            case 'PLN':
                $currency = Currency::PLN();
                break;
            case 'USD':
                $currency = Currency::USD();
                break;
            case 'EUR':
                $currency = Currency::EUR();
                break;
            default:
                $currency = Currency::NO_CHANGED();
                break;
        }

        return $currency;
    }
}
