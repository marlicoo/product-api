<?php

namespace App\Domain\ValueObject;

use App\Domain\Exception\IdentifierInvalidArgument;
use Ramsey\Uuid\Uuid;

class Identifier
{
    /** @var string */
    private $id;

    /**
     * Identifier constructor.
     * @param string $id
     * @throws IdentifierInvalidArgument
     */
    public function __construct(string $id)
    {
        if (!$this->isValidUuid($id)) {
            throw IdentifierInvalidArgument::forUuid($id);
        }

        $this->id = $id;
    }

    /**
     * @param Identifier $identifier
     * @return bool
     */
    public function equals(Identifier $identifier): bool
    {
        return $this->id === $identifier->id;
    }

    /**
     * @param string $id
     * @return Identifier
     * @throws IdentifierInvalidArgument
     */
    public static function UUID(string $id): Identifier
    {
        return new self($id);
    }

    /**
     * @param string $id
     * @return bool
     */
    private function isValidUuid(string $id): bool
    {
        return preg_match('/\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/', $id, $matches);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->id;
    }
}
