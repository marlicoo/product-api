<?php

namespace App\Domain\Model\Cart;


use App\Domain\Exception\CartProductLimitExpire;
use App\Domain\Model\DomainModel;
use App\Domain\Model\Product\Product;
use App\Domain\Service\ProductLimit\CartProductLimit;
use App\Domain\ValueObject\Identifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Cart extends DomainModel
{
    /** @var Collection */
    private $products;

    /** @var int */
    private $productLimit;

    /**
     * Cart constructor.
     * @param Identifier $id
     * @param CartProductLimit $productLimit
     */
    public function __construct(Identifier $id, CartProductLimit $productLimit)
    {
        parent::__construct($id);

        $this->products = new ArrayCollection();
        $this->productLimit = $productLimit->limit();
    }


    /**
     * @param Identifier $id
     * @param CartProductLimit $productLimitStrategy
     * @return Cart
     */
    public static function create(Identifier $id, CartProductLimit $productLimitStrategy): Cart
    {
        return new self($id, $productLimitStrategy);
    }

    /**
     * @param $product
     * @throws CartProductLimitExpire
     */
    public function addProduct(Product $product): void
    {
        if ($this->products->count() === $this->productLimit) {

            throw CartProductLimitExpire::forLimit($this->productLimit);
        }

        $this->products->add($product);
    }

    /**
     * @param Product $product
     */
    public function removeProduct(Product $product): void
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }
    }
}
