<?php

namespace App\Domain\Model;


use App\Domain\ValueObject\Identifier;

abstract class DomainModel
{
    /** @var Identifier */
    protected $id;

    /**
     * DomainModel constructor.
     * @param Identifier $id
     */
    public function __construct(Identifier $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
