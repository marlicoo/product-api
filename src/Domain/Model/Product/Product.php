<?php

namespace App\Domain\Model\Product;


use App\Domain\Model\DomainModel;
use App\Domain\ValueObject\Money;
use App\Domain\ValueObject\Identifier;

class Product extends DomainModel
{
    /** @var string */
    private $title;

    /** @var Money */
    private $price;

    /** @var string */
    private $status;

    /**
     * Product constructor.
     * @param Identifier $id
     * @param string $title
     * @param Money $price
     */
    public function __construct(Identifier $id, $title, Money $price, $status)
    {
        parent::__construct($id);

        $this->title = $title;
        $this->price = $price;
        $this->status = $status;
    }


    /**
     * @param Identifier $id
     * @param string $title
     * @param Money $price
     * @return Product
     */
    public static function create(Identifier $id, string $title, Money $price): Product
    {
        return new self($id, $title, $price, ProductStatusType::ACTIVE);
    }

    public function remove(): void
    {
        $this->status = ProductStatusType::INACTIVE;
    }

    /**
     * @param $newTitle
     */
    public function changeTitle($newTitle)
    {
        if (!empty($newTitle)) {
            $this->title = $newTitle;
        }
    }

    /**
     * @param $newPrice
     */
    public function changePrice(Money $newPrice)
    {
        $this->price = $newPrice;
    }
}
