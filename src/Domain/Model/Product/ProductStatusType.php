<?php

namespace App\Domain\Model\Product;


use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;


final class ProductStatusType extends AbstractEnumType
{
    public const ACTIVE = 'active';
    public const INACTIVE = 'inactive';

    /**
     * @var array
     */
    protected static $choices = [
        self::ACTIVE => 'active',
        self::INACTIVE => 'inactive',
    ];
}
