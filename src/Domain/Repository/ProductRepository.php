<?php

namespace App\Domain\Repository;


use App\Application\Exception\ResourceNotFound;
use App\Domain\Model\Product\Product;
use App\Domain\ValueObject\Identifier;

interface ProductRepository
{
    public function create(Product $product);

    public function update(Product $product);

    /**  @throws ResourceNotFound */
    public function findById(Identifier $productId);

    /**  @throws ResourceNotFound */
    public function findByIdAndStatus(Identifier $productId, string $status);

    public function isProductInCarts(Identifier $productId);
}