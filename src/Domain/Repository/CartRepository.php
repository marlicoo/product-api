<?php

namespace App\Domain\Repository;


use App\Domain\Model\Cart\Cart;
use App\Domain\ValueObject\Identifier;

interface CartRepository
{
    public function create(Cart $cart);

    public function findById(Identifier $cartId);

    public function update(Cart $cart);

    public function isProductInCart(Identifier $cartId, Identifier $productId): bool;
}
