<?php

namespace App\Domain\Exception;


use App\Domain\ValueObject\Identifier;

class ProductCanNotBeRemoved extends \RuntimeException implements DomainException
{
    /**
     * @param Identifier $id
     * @return ProductCanNotBeRemoved
     */
    public static function forId(Identifier $id): ProductCanNotBeRemoved
    {
        return new self(sprintf('Product id %s can not be removed because it is being processed in the open cart', $id));
    }
}

