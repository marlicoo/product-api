<?php

namespace App\Domain\Exception;


class CartProductLimitExpire extends \RuntimeException implements DomainException
{
    /**
     * @param int $limit
     * @return CartProductLimitExpire
     */
    public static function forLimit(int $limit): CartProductLimitExpire
    {
        return new self(sprintf('Limit %d of product in cart has been expired', $limit));
    }
}
