<?php

namespace App\Domain\Exception;


class MoneyInvalidArgument extends \InvalidArgumentException implements DomainException
{
    /**
     * @param string $currency
     * @return MoneyInvalidArgument
     */
    public static function forCurrency(string $currency): MoneyInvalidArgument
    {
        return new self(sprintf('Currency %s is not valid', $currency));
    }

    /**
     * @param string $currency
     * @return MoneyInvalidArgument
     */
    public static function forNegativeAmount(string $currency): MoneyInvalidArgument
    {
        return new self(sprintf('Currency %s is not valid', $currency));
    }

    /**
     * @param string $currency
     * @return MoneyInvalidArgument
     */
    public static function forNotSupportedCurrency(string $currency): MoneyInvalidArgument
    {
        return new self(sprintf('Currency %s is not supported', $currency));
    }
}
