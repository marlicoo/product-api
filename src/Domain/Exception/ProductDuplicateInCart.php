<?php

namespace App\Domain\Exception;


use App\Domain\ValueObject\Identifier;

class ProductDuplicateInCart extends \RuntimeException implements DomainException
{
    /**
     * @param Identifier $id
     * @return ProductDuplicateInCart
     */
    public static function forId(Identifier $id): ProductDuplicateInCart
    {
        return new self(sprintf('Product id %s has been already added to the cart', $id));
    }
}
