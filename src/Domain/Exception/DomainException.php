<?php

namespace App\Domain\Exception;

interface DomainException
{
    public function getMessage();
}