<?php

namespace App\Domain\Exception;


class IdentifierInvalidArgument extends \InvalidArgumentException implements DomainException
{
    /**
     * @param string $id
     * @return IdentifierInvalidArgument
     */
    public static function forUuid(string $id): IdentifierInvalidArgument
    {
        return new self(sprintf('Id %s is not valid uuid type', $id));
    }
}
