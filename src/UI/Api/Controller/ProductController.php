<?php

namespace App\UI\Api\Controller;


use App\Application\Enum\Resource;
use App\Application\Response\JsonResourceResponse;
use App\Application\UseCase\Product\Command\ProductChangeDetails;
use App\Application\UseCase\Product\Command\ProductCreate;
use App\Application\UseCase\Product\Command\ProductRemove;
use App\Application\UseCase\Product\Query\Filter\PaginationFilter;
use App\Application\UseCase\Product\Query\ProductQuery;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class ProductController extends Controller
{
    /** @var CommandBus */
    private $commandBus;

    /** @var ProductQuery */
    private $productQuery;

    /**
     * CartController constructor.
     * @param CommandBus $commandBus
     * @param ProductQuery $productQuery
     */
    public function __construct(CommandBus $commandBus, ProductQuery $productQuery)
    {
        $this->commandBus = $commandBus;
        $this->productQuery = $productQuery;
    }

    /**
     * @Route("/products", name="product_create")
     * @Method("POST")
     * @param ProductCreate $command
     * @ParamConverter("command", class="App\Application\UseCase\Product\Command\ProductCreate", converter="command_converter")
     *
     * @SWG\Response(
     *   response=201,
     *   description="Create new product",
     *   @SWG\Schema(
     *      example= {
     *        "title": "super3",
     *        "price": "21.99 PLN",
     *        "_links": {
     *        "self": {
     *            "href": "/products/2a249768-0706-415f-a032-08c367189633"
     *             },
     *        "products": {
     *            "href": "/products"
     *            }
     *          }
     *       })
     *    )
     * @SWG\Response(
     *     response=422,
     *     description="Returns when params validation failure")
     * @SWG\Parameter(
     *          name="ProductCreate",
     *          in="body",
     *          type="object",
     *          description="Product uuid",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="id", type="uuid"),
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="amount", type="string"),
     *              @SWG\Property(property="currency", type="string"),
     * ))
     * @SWG\Tag(name="v1")
     *
     * @return JsonResponse
     */
    public function createAction(ProductCreate $command): JsonResponse
    {
        $this->commandBus->handle($command);

        return JsonResourceResponse::created(Resource::PRODUCT(), $command);
    }

    /**
     * @Route("/products/{id}", name="product_change_details", requirements=
     *     {
     *         "_format": "json",
     *     })
     * @Method("PUT")
     * @ParamConverter("command", class="App\Application\UseCase\Product\Command\ProductChangeDetails", converter="command_converter")
     * @SWG\Response(
     *   response=200,
     *   description="Update product",
     *    )
     * @SWG\Response(
     *     response=422,
     *     description="Returns when params validation failure")
     * @SWG\Parameter(
     *          name="ProductChangeDetails",
     *          in="body",
     *          type="object",
     *          description="Params are set as optional but when none of them will be provided validation error will occur",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="amount", type="string"),
     *              @SWG\Property(property="currency", type="string"),
     * ))
     * @SWG\Tag(name="v1")
     * @return JsonResponse
     */
    public function updateAction(ProductChangeDetails $command): JsonResponse
    {
        $this->commandBus->handle($command);

        return JsonResourceResponse::updated();
    }

    /**
     * @Route("/products/{id}", name="product_delete", requirements=
     *     {
     *         "_format": "json",
     *     })
     * @Method("DELETE")
     * @ParamConverter("command", class="App\Application\UseCase\Product\Command\ProductRemove", converter="command_converter")
     * @param ProductRemove $command
     *
     * @SWG\Response(
     *   response=204,
     *   description="Remove product")
     * @SWG\Response(
     *     response=422,
     *     description="Returns when params validation failure")
     * @SWG\Parameter(
     *     name="product_remove",
     *     in="body",
     *     @Model(type=ProductCreate::class)
     * )
     * @SWG\Tag(name="v1")
     *
     * @return JsonResponse
     */
    public function deleteAction(ProductRemove $command): JsonResponse
    {
        $this->commandBus->handle($command);

        return JsonResourceResponse::deleted();
    }

    /**
     * @Route("/products", name="product_list")
     *
     * @Method("GET")
     * @param Request $request
     *
     * @SWG\Response(
     *   response=200,
     *   description="Return list of products",
     *   @SWG\Schema(
     *      example=
     *     {
     *      "limit": 3,
     *      "page": 1,
     *      "_links": {
     *          "self": {
     *              "href": "/products"
     *              },
     *          "next": {
     *              "href": "/products?page=2&&limit=3"
     *              }
     *          },
     *      "_embedded": {
     *          "product":
     *              {
     *                  "id": "2a249768-0706-415f-a032-08c365189641",
     *                  "title": "Super2",
     *                  "price": "14.55 USD",
     *                  "_links": {
     *                      "self": {
     *                          "href": "/products/2a249768-0706-415f-a032-08c365189641"
     *                          }
     *                      }
     *                  }
     *              }
     *          }
     *    )
     * )
     * @SWG\Tag(name="v1")
     * @return JsonResponse
     */
    public function listAction(Request $request): JsonResponse
    {
        $view = $this->productQuery->findAll(new PaginationFilter(
            $request->query->get('limit'),
            $request->query->get('page'))
        );

        return JsonResourceResponse::resourceList($view, Resource::PRODUCT());
    }
}
