<?php

namespace App\UI\Api\Controller;


use App\Application\Enum\Resource;
use App\Application\Response\JsonResourceResponse;
use App\Application\UseCase\Cart\Command\AddProduct;
use App\Application\UseCase\Cart\Command\CartCreate;
use App\Application\UseCase\Cart\Command\RemoveProduct;
use App\Application\UseCase\Cart\Query\CartQuery;
use App\Domain\Exception\IdentifierInvalidArgument;
use App\Domain\ValueObject\Identifier;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

class CartController extends Controller
{
    /** @var CommandBus */
    private $commandBus;

    /** @var CartQuery */
    private $cartQuery;

    /**
     * CartController constructor.
     * @param CommandBus $commandBus
     * @param CartQuery $cartQuery
     */
    public function __construct(CommandBus $commandBus, CartQuery $cartQuery)
    {
        $this->commandBus = $commandBus;
        $this->cartQuery = $cartQuery;
    }

    /**
     * @Route("/carts", name="cart_create")
     * @Method("POST")
     * @param CartCreate $command
     * @ParamConverter("command", class="App\Application\UseCase\Cart\Command\CartCreate", converter="command_converter")
     *
     * @SWG\Response(
     *   response=201,
     *   description="Create new cart",
     *   @SWG\Schema(
     *      example= {
     *        "_links": {
     *        "self": {
     *        "href": "/carts/2a249768-0706-415f-a032-08c363889430"
     *         },
     *        "products": {
     *           "href": "/products"
     *         },
     *        }
     *      })
     *    )
     * @SWG\Response(
     *     response=422,
     *     description="Returns when params validation failure")
     *   @SWG\Parameter(
     *          name="CartCreate",
     *          in="body",
     *          type="object",
     *          description="Cart uuid",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="id", type="uuid"),
     * ))
     * @SWG\Tag(name="v1")
     *
     * @return JsonResponse
     */
    public function createAction(CartCreate $command): JsonResponse
    {
        $this->commandBus->handle($command);

        return JsonResourceResponse::created(Resource::CART(), $command);
    }

    /**
     * @Route("/carts/{id}/products", name="cart_add_product")
     * @Method("POST")
     * @param AddProduct $command
     * @ParamConverter("command", class="App\Application\UseCase\Cart\Command\AddProduct", converter="command_converter")
     *
     * @SWG\Response(
     *   response=200,
     *   description="Add product to cart",
     *   @SWG\Schema(
     *      example= {
     *        "_links": {
     *        "self": {
     *        "href": "/carts/2a249768-0706-415f-a032-08c363889430"
     *         },
     *        "products": {
     *           "href": "/products"
     *         },
     *        }
     *      })
     *    )
     * @SWG\Response(
     *     response=422,
     *     description="Returns when params validation failure")
     * @SWG\Parameter(
     *          name="AddProduct",
     *          in="body",
     *          type="string",
     *          description="Cart data",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="productId", type="uuid"),
     * ))
     * @SWG\Tag(name="v1")
     * @return JsonResponse
     */
    public function addProductAction(AddProduct $command): JsonResponse
    {
        $this->commandBus->handle($command);

        return JsonResourceResponse::addedToResource(Resource::CART(), $command, Resource::PRODUCT(), $command->productId());
    }

    /**
     * @Route("/carts/{id}/products/{productId}", name="cart_remove_product", requirements=
     *     {
     *         "_format": "json",
     *     })
     * @Method("DELETE")
     * @param RemoveProduct $command
     * @ParamConverter("command", class="App\Application\UseCase\Cart\Command\RemoveProduct", converter="command_converter")
     *
     * @SWG\Response(
     *   response=200,
     *   description="Remove product from the cart",
     *   @SWG\Schema(
     *      example= {
     *        "_links": {
     *        "self": {
     *        "href": "/carts/2a249768-0706-415f-a032-08c363889430"
     *         },
     *        "products": {
     *           "href": "/products"
     *         },
     *        }
     *      })
     *    )
     * @SWG\Response(
     *     response=422,
     *     description="Returns when params validation failure")
     * @SWG\Parameter(
     *     name="cart_product_remove",
     *     in="body",
     *     @Model(type=RemoveProduct::class)
     * )
     * @SWG\Tag(name="v1")
     *
     * @return JsonResponse
     */
    public function removeProductAction(RemoveProduct $command): JsonResponse
    {
        $this->commandBus->handle($command);

        return JsonResourceResponse::removedFromResource(Resource::CART(), $command);
    }

    /**
     * @Route("/carts/{id}", name="cart_product_list")
     * @Method("GET")
     * @param Request $request
     *
     * @SWG\Response(
     *   response=200,
     *   description="Return list of products",
     *   @SWG\Schema(
     *      example=
     *     {
     *      "valueOfCart": "6.98 USD",
     *      "_links": {
     *          "self": {
     *              "href": "/carts/2a249768-0706-415f-a032-08c363189632"
     *              },
     *          "products": {
     *              "href": "/products"
     *              }
     *           },
     *       "_embedded": {
     *          "product": {
     *              {
     *               "id": {},
     *              "title": "Baldur`s Gate",
     *              "price": "3.99 USD",
     *                  "_links": {
     *                      "self": {
     *                          "href": "products/0145597c-b23a-45ef-b213-96d350636f83"
     *                          }
     *                      }
     *                },
     *              }
     *           }
     *        }
     *    )
     * )
     * @SWG\Tag(name="v1")
     *
     * @return JsonResponse
     * @throws IdentifierInvalidArgument
     */
    public function cartWithProductAction($id): JsonResponse
    {
        $view = $this->cartQuery->cart(Identifier::UUID($id));

        return JsonResourceResponse::resource($view, Resource::CART());

    }
}
