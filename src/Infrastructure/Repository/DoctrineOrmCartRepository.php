<?php

namespace App\Infrastructure\Repository;


use App\Application\Exception\ResourceNotFound;
use App\Domain\Model\Cart\Cart;
use App\Domain\Repository\CartRepository;
use App\Domain\ValueObject\Identifier;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class DoctrineOrmCartRepository extends EntityRepository implements CartRepository
{
    public const RESOURCE_NAME = 'Cart';

    /**
     * @param Cart $cart
     */
    public function create(Cart $cart): void
    {
        $this->_em->persist($cart);
        $this->_em->flush();
    }

    /**
     * @param $cartId
     * @return object
     * @throws ResourceNotFound
     */
    public function findById(Identifier $cartId)
    {
        if(null === $cart = $this->_em->find(Cart::class, $cartId)){
            throw ResourceNotFound::forResourceTypeAndId($cartId, self::RESOURCE_NAME);
        }

        return $cart;
    }

    /**
     * @param Cart $cart
     */
    public function update(Cart $cart)
    {
        $this->_em->persist($cart);
        $this->_em->flush();
    }

    public function isProductInCart(Identifier $cartId, Identifier $productId): bool
    {
        $sql = 'SELECT * FROM carts_products WHERE carts_products.product_id=:productId AND carts_products.cart_id=:cartId';

        return !$this->_em->getConnection()->executeQuery($sql, ['productId' => $productId, 'cartId' => $cartId])
            ->fetch() ? false : true;
    }
}
