<?php

namespace App\Infrastructure\Repository;


use App\Application\Exception\ResourceNotFound;
use App\Domain\Model\Product\Product;
use App\Domain\Repository\ProductRepository;
use App\Domain\ValueObject\Identifier;
use Doctrine\ORM\EntityRepository;

class DoctrineOrmProductRepository extends EntityRepository implements ProductRepository
{
    public const RESOURCE_NAME = 'Product';

    /**
     * @param Product $product
     */
    public function create(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    /**
     * @param $productId
     * @return object
     * @throws ResourceNotFound
     */
    public function findById(Identifier $productId)
    {
        if(null === $product = $this->_em->find(Product::class, $productId)){
            throw ResourceNotFound::forResourceTypeAndId($productId, self::RESOURCE_NAME);
        }

        return $product;
    }

    /**
     * @param $productId
     * @return object
     * @throws ResourceNotFound
     */
    public function findByIdAndStatus(Identifier $productId, string $status)
    {
        $product = $this->findBy([
            'id' => $productId,
            'status' => $status
        ]);

        if(empty($product)){
            throw ResourceNotFound::forResourceTypeAndId($productId, self::RESOURCE_NAME);
        }

        return $product[0];
    }

    /**
     * @param Product $product
     */
    public function update(Product $product)
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    /**
     * @param Identifier $productId
     * @return bool
     */
    public function isProductInCarts(Identifier $productId): bool
    {
        $sql = 'SELECT * FROM carts_products WHERE carts_products.product_id=:id';

        return !$this->_em->getConnection()->executeQuery($sql, ['id' => $productId])->fetch() ? false : true;
    }
}
