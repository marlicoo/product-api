<?php

namespace App\Infrastructure\DataFixture;


use App\Domain\Exception\IdentifierInvalidArgument;
use App\Domain\Model\Product\Product;
use App\Domain\ValueObject\Identifier;
use App\Domain\ValueObject\Money;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class ProductFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @throws IdentifierInvalidArgument
     */
    public function load(ObjectManager $manager)
    {
        $titles = ['Fallout', 'Don`t Starve', 'Baldur`s Gate', 'Icewind Dale', 'Bloodborne', 'Dragon'];
        $ids = [
            '513c6458-25f2-4372-9e12-b2428c8019bb',
            '613c6458-25f2-4372-9e12-b2428c8019bb',
            '713c6458-25f2-4372-9e12-b2428c8019bb',
            '813c6458-25f2-4372-9e12-b2428c8019bb',
            '913c6458-25f2-4372-9e12-b2428c8019bb',
            '013c6458-25f2-4372-9e12-b2428c8019bb'

        ];

        $startPrice = 1.99;

        for ($i = 0; $i < 6; $i++) {
            $product = Product::create(
                new Identifier($ids[$i]),
                $titles[$i],
                Money::USD($startPrice + $i));

            $manager->persist($product);
        }

        $manager->flush();
    }
}
